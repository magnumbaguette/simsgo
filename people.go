package sims

type Professions interface {
	Trash
	SoftwareEngineering
}

type People struct {
	Trashman
	SoftwareEngineer
}

type Person struct {
	Name string
	Age int
}

type Trashman struct {
	Person
}

type SoftwareEngineer struct {
	Person
	SoftwareName string
	SoftwareLang string
}
