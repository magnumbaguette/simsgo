package sims

type SoftwareEngineering interface {
	Designsoftware(name string, lang string) string
	Show() SoftwareEngineer
}

func (s *SoftwareEngineer) Designsoftware(name string, lang string) string {
	s.SoftwareName = name
	s.SoftwareLang = lang
	return "Writing the " + name + " software using " + lang 
}

func (s SoftwareEngineer) Show() SoftwareEngineer{
	return s
}


func CreateSoftwareEngineer(name string, age int) SoftwareEngineer{
	person := Person{Name: name, Age: age,}
	return SoftwareEngineer{Person: person,}
}