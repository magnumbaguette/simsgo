package sims

type Trash interface {
	TakeTrash(name Trashman) string
}

func (p Person) TakeTrash() string{
	return  p.Name + " is taken by the trashman"
}

func CreateTrashman(name string, age int) Trashman{
	return Trashman{Person: Person{Name: name, Age: age,},}
}